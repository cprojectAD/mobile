package com.mobiletargil4.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.HashMap;

/**
 * the activity for the chat,allowing to get messages from the web server and post
 * your on messages.
 * messages are in a scroll view.
 */
public class ScrollingActivity extends AppCompatActivity implements PosterActivity {

    private static String loginURL = "http://advprog.cs.biu.ac.il:8080/WebApplication1/MessageBoard";
    private static TextView txt; //the chat window
    private static TextView user_txt;//the user txt
    private static  ScrollView bar;
    private UserLoginTask mAuthTask = null;


    Button send;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

       txt =  (TextView)findViewById(R.id.scroll);
        txt.setSingleLine(false);

         bar = (ScrollView) findViewById(R.id.nice);

        user_txt =  (TextView)findViewById(R.id.editText);
         send = (Button) findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                post_data(user_txt.getText().toString()+"\n"); //post with the text in the bar
            }
        });


        bar.post(new Runnable() {//force scroll down the button of the messages(newst message)
            @Override
            public void run() {
                bar.fullScroll(View.FOCUS_DOWN);
            }
        });

            post_data("read_msg");//update the view first time reading.



    }

    /**
     * posts the message the user sent to the chat
     * @param data message from the user or "read_msg" if getting updates only.
     */
    private void post_data(String data) {
        HashMap<String,String> map = new HashMap<String,String>();
        map.put("message",data);
         mAuthTask = new UserLoginTask(this,map);
        mAuthTask.execute(loginURL);

    }
    //called when user login failed
    public void failed(String answer) {
        txt.setText(answer);
    }
    //after login swap activity
    public void advance(String answer){
        txt.setText(answer);
        user_txt.setText("");
        bar.post(new Runnable() {
            @Override
            public void run() {
                bar.fullScroll(View.FOCUS_DOWN);
            }
        });

    }










}
