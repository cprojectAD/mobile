package com.mobiletargil4.myapplication;

import android.app.Application;

import java.net.CookieHandler;
import java.net.CookieManager;


/**
 * needed to register cookies
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        CookieHandler.setDefault(new CookieManager());
    }
}
