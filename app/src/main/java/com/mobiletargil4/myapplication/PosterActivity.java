package com.mobiletargil4.myapplication;

import android.content.Intent;
import android.widget.Toast;

/*
    implemented by activities which post to the server,using a posterTask
 */
public interface PosterActivity {

    //called if posting to the server failed(wrong post format or the action itself failed.
    public void failed(String answer) ;

    /**
     * called if the post was succsusful
     * @param answer the server answer from doPost
     */
    public void advance(String answer);
}
