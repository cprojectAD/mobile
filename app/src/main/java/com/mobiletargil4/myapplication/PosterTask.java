package com.mobiletargil4.myapplication;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


/**
 * Asynctask that implement the public method POST.
 * this asynctask posts to the url the data given
 */
public abstract class PosterTask extends AsyncTask<String, String, String> {
    //keep session from server so it can track our username
    public static String cookie = null;

    /**
     * Posts the set of parameters key:value in postDataparams to the requestURL
     * @param requestURL url to post too
     * @param postDataParams params to post
     * @return
     */
    public String  POST(String requestURL,
                                   HashMap<String, String> postDataParams) {

        URL url;
        String response = "";
        HttpURLConnection conn = null;
        try {
            url = new URL(requestURL);

             conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(15000);
            conn.setReadTimeout(15000);
            //allow posting data and reading data back.
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            //if we are already loged,set the cookie so the server can regonize us.
            if(cookie != null)
                conn.setRequestProperty("Cookie", UserLoginTask.cookie);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(data2String(postDataParams));

            writer.flush();
            writer.close();
            os.close();

            String line;
            //read answer from the server(the messages)
            BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line=br.readLine()) != null) {
                response+=line+"\n";
            }


        } catch (Exception e) {
            e.printStackTrace();
            response = "exception";
        }

        //first time login in
        if(cookie == null)
            cookie = conn.getHeaderField("Set-Cookie");

        return response;
    }

    /**
     * helper functio nto decode post data to string.
     * @param params
     * @return
     * @throws UnsupportedEncodingException
     */
    private String data2String(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }




}