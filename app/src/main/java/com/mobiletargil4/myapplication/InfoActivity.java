package com.mobiletargil4.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;


/**
 * Simple splash with 5 rotating messages
 */
public class InfoActivity extends AppCompatActivity {
    Button butt;
    //rotating messages
    String[] strings = {"Waking Up","Streching","Checking for \n irrelevent updates","Just waisting your time","Lunching"};
    int i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);


        butt = (Button) findViewById(R.id.dummy_button);
        i = 0;
        startMsg();//start messages
    }

    /**
     * starts to rotate the 5 messages from strings.each message displied for 1 sec.
     */
    private void startMsg(){
        //thread for rotating messaged.need a new thread so the screen doesnt black out.
        Thread tLoader = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    while (i <= 4) {

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                butt.setText(strings[i]);
                            }
                        });
                        //displa message for 1 sec
                        Thread.sleep(1000,0);
                        i++;
                    }


                    //after finishing all the messaged, jump to the swapActivity
                  Intent k = new Intent(InfoActivity.this,SwappActivity.class);
                   startActivity(k);
                }
                catch (Exception ex)
                {

                }
            }
        });

       tLoader.start();//starts the task on a different thread so the screen doesnt black out.
    }
}
