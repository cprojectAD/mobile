package com.mobiletargil4.myapplication;

import android.util.Log;

import java.util.HashMap;

/**
 * the class which sends post requests to the server
 */
public class UserLoginTask extends PosterTask{

    private final PosterActivity activity;
    private String user,pass;
    private HashMap<String, String> map; //data to post
    public UserLoginTask(PosterActivity activity,HashMap<String, String> map) {
        this.map = map;
        this.activity = activity;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }


    @Override
    protected String doInBackground(String... params) {

        String urlString = params[0]; // URL to call

        return  POST(urlString,map);


    }


    /**
     * calls the failed or advance function on the calling activity acroding to the string
     * which the webserver asnwered.
     * @param result answer from the web server after posting.
     */
    @Override
    protected void onPostExecute(String result) {
        Log.i("doInBackground", Thread.currentThread().getName());
        if(result.toLowerCase().contains("wrong"))
            activity.failed(result);

        else activity.advance(result);

    }


}